package com.nevermore.messager.bean;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
public class MessageInfo {
    private String hostAddr;
    private Message message;
}
