package com.nevermore.messager.bean;


import com.google.common.base.MoreObjects;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * tb_msg表的映射对象
 */
@Getter
@Setter
@NoArgsConstructor
public class TbMsg {
    private Integer id;
    private String userName;
    private String hostName;
    private String hostAddress;
    private String macAddress;
    private String localUserName;
    private String localHostName;
    private String localHostAddress;
    private String localMacAddress;
    private String type;
    private String messageType;
    private String messageText;
    private String sendTime;
    private String receiveTime;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("type", type)
                .add("messageType", messageType)
                .add("messageText", messageText)
                .add("userName", userName)
                .toString();
    }
}
