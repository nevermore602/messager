package com.nevermore.messager.bean;

import com.google.common.base.Charsets;
import com.google.common.base.MoreObjects;
import com.google.common.primitives.Bytes;
import com.google.common.primitives.Longs;
import com.google.common.primitives.Shorts;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Arrays;

/**
 * 一个包由【消息头部】、【消息正文】、【其他信息】三部分组成
 * 消息协议格式：
 * |<---------------------------------- 消息头部 -------------------------------->|<-消息正文->|<--其他信息->|
 * ------------------------------------------------------------------------------------------------------
 * | 包序号(8byte)| 命令字(1byte)| 分包个数(1byte)| 分包序号(1byte)| 消息正文长度(2byte)|  消息正文  | 用户名:主机名|
 * ------------------------------------------------------------------------------------------------------
 * 包序号：占8Byte，采用当前毫秒数转换为字节数组，这也是包的发送时间
 * 命令字：1个byte，可以表示127种命令；每个命令字在Config类中定义
 * 分包个数：1 byte，整包一次发送时，此字段为1
 * 分包序号：1 byte，从0开始编号，整包一次发送时，此字段为0
 * 消息正文长度：2byte, 一个short类型
 */
@Getter
@Setter
@NoArgsConstructor
public class Message {
    private long seq; //包序号
    private byte cmd; // 命令字，表明消息的类型
    private byte subNum; // 分包个数
    private byte subSeq; // 分包序号
    private String userName;
    private String hostName;
    private byte[] data; //消息正文
    private short dataLength;
    private String otherInfo;

    public Message(byte cmd, String userName, String hostName, String message) {
        this(cmd,  (byte) 1, (byte) 0, userName, hostName, message);
    }

    public Message(byte cmd, byte subNum, byte subSeq, String userName, String hostName, String message) {
        this(System.currentTimeMillis(), cmd, subNum, subSeq, userName, hostName, message);
    }

    public Message(long seq, byte cmd, byte subNum, byte subSeq, String userName, String hostName, String message) {
        this(seq, cmd, subNum, subSeq, userName, hostName, message.getBytes(Charsets.UTF_8));
    }

    public Message(long seq, byte cmd, byte subNum, byte subSeq, String userName, String hostName, byte[] data) {
        this.seq = seq;
        this.cmd = cmd;
        this.subNum = subNum;
        this.subSeq = subSeq;
        this.data = data;
        this.hostName = hostName;
        this.userName = userName;
        this.dataLength = (short) this.data.length;
        this.otherInfo = this.userName + ":" + this.hostName;
    }

    public String getMessage() {
        return new String(data, Charsets.UTF_8);
    }

    // 返回头部信息字节数
    public int getHeaderByteLen() {
        return 8 + 3 + 2;
    }

    // 获取其他信息内容
    public int getOtherByteLen() {
        return otherInfo.getBytes().length;
    }

    // 获取消息内容字节数
    public int getDataByteLen() {
        return data.length;
    }

    // 获取整个消息的字节数
    public int getAllByteLen() {
        return getHeaderByteLen() + getDataByteLen() + getOtherByteLen();
    }

    public Message buildSubMessage(byte num, byte index, int size) {
        int toIndex = Math.min(data.length, (index + 1) * size);
        return new Message(seq, cmd, num, index, userName, hostName,
                Arrays.copyOfRange(data, index * size, toIndex));
    }

    public byte[] toBytes() {
        byte[] seqBytes = Longs.toByteArray(this.seq);
        byte[] threeBytes = new byte[]{cmd, subNum, subSeq};
        byte[] otherBytes = otherInfo.getBytes(Charsets.UTF_8);
        byte[] dataLengthBytes = Shorts.toByteArray((short) data.length);
        return Bytes.concat(seqBytes, threeBytes, dataLengthBytes, data, otherBytes);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("dataLength", data.length)
                .add("seq", seq)
                .add("subNum", subNum)
                .add("subSeq", subSeq)
                .add("cmd", cmd)
                .add("userName", userName)
                .add("hostName", hostName)
                .add("message", getMessage())
                .toString();
    }
}