package com.nevermore.messager.bean;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User {
    private String hostName; //主机名，用户主机的名称，如：Dell-PC
    private String hostAddress; //主机地址，用户主机的IP地址，形如：192.168.1.100
    private String macAddress; //主机硬件地址
    private String userName; //用户自定义的登录本软件的用户名，如：NeverMore，如果没有设置，则默认为loginName的值
    private String remark; //个人描述信息，预留
}