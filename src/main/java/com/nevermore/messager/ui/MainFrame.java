package com.nevermore.messager.ui;

import com.google.common.collect.Sets;
import com.google.common.util.concurrent.FutureCallback;
import com.nevermore.messager.bean.User;
import com.nevermore.messager.handler.MessageReceiver;
import com.nevermore.messager.handler.MessageSender;
import com.nevermore.messager.util.*;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.*;
import java.net.SocketException;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.ConcurrentLinkedQueue;


public class MainFrame extends JFrame implements ActionListener {
    private static final long serialVersionUID = 1L;
    private static MainFrame instance = null;
    private ConcurrentLinkedQueue<TalkFrame> talkFrames;  //保存所有打开的聊天框
    private Set<User> users; //在线用户集合
    private User self; //自身的信息
    private JLabel label;
    private JList<User> userList;
    private JButton refresh;

    private MainFrame() {
        try {
            DBUtils.checkMsgDB();
        } catch (Exception e) {
            LogUtils.error("创建数据库失败！", e);
            UIUtils.showErroMessage(this, "创建数据库失败！");
            return;
        }
        talkFrames = new ConcurrentLinkedQueue<>();
        users = Sets.newHashSet();

        initComponents();

        // 在线程池中加载本机信息，加载成功后构建消息接收器、消息发送器、自身用户等
        TaskUtils.executeWithCallback(() -> {
            String localHostInfo = NetUtils.getLocalHostInfo();
            LogUtils.info("获取到本机信息：%s", localHostInfo);
            if (localHostInfo.length() > 2) {
                String[] localHostInfoArr = localHostInfo.split(";");
                Config.LOCAL_MAC_ADDR = localHostInfoArr[0];
                Config.LOCAL_HOST_ADDR = localHostInfoArr[1];
                Config.LOCAL_HOST_NAME = localHostInfoArr[2];
            }
            return localHostInfo;
        }, new FutureCallback<String>() {
            @Override
            public void onSuccess(String result) {
                self = new User();
                self.setHostName(Config.LOCAL_HOST_NAME);
                self.setHostAddress(Config.LOCAL_HOST_ADDR);
                self.setMacAddress(Config.LOCAL_MAC_ADDR);
                self.setUserName(Config.LOCAL_USER_NAME_DEFAULT);  //此处预留，将来通过读取配置文件加载用户的设置，如果未设置则等于loginName
                self.setRemark(""); //此处预留，将来通过读取配置文件加载用户的设置
                LogUtils.info(self.toString());
                MainFrame.getInstance().addUser(self);
                MessageReceiver msgReceiver;
                try {
                    //初始化消息接收器
                    msgReceiver = MessageReceiver.getInstance();
                    // 初始化消息发送器
                    MessageSender.init();
                } catch (SocketException e) {
                    UIUtils.showErroMessage(MainFrame.getInstance(), "消息接收器或发送器启动失败！" + e.getMessage());
                    return;
                }
                // 启动消息接收线程
                TaskUtils.execute(msgReceiver);
                //发送登陆广播消息
                MessageSender.sendLoginBroadcast();
            }

            @Override
            public void onFailure(Throwable e) {
                LogUtils.error("获取本机信息失败！", e);
                UIUtils.showErroMessage(MainFrame.getInstance(), "获取本机信息失败！" + e.getMessage());
            }
        });

    }

    public static void init() {
        newInstance();
    }

    private static void newInstance() {
        if (instance == null) {
            instance = new MainFrame();
        }
    }

    public static MainFrame getInstance() {
        return instance;
    }

    private void initComponents() {
        userList = new JList<>(new Vector<>(users));
        userList.setCellRenderer(new MyListCellRenderer());
        label = new JLabel(UIUtils.getLabelString(users.size()));
        userList.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                MyListCellRenderer rd = (MyListCellRenderer) (userList.getCellRenderer());
                rd.setBorder(new LineBorder(Color.red, 4, false));
                if (e.getClickCount() == 2) {   //双击时
                    User user = userList.getSelectedValue();
                    if (user != null) {
                        TalkFrame tf = MainFrame.this.getTalkFrame(user);
                        if (tf == null) {
                            TalkFrame newTF = new TalkFrame(user);
                            MainFrame.this.addTalkFrame(newTF);
                            newTF.setVisible(true);
                        } else {
                            tf.setVisible(true);
                        }
                    }
                }
            }
        });
        refresh = new JButton("刷新", UIUtils.getImageIcon("images/refresh.png"));
        refresh.setFocusable(false);
        refresh.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        refresh.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        refresh.addActionListener(this);
        JToolBar toolBar = new JToolBar();
        toolBar.setBorderPainted(false);
        toolBar.add(refresh);
        JPanel center = new JPanel(new BorderLayout(0, 5));
        center.add(label, BorderLayout.NORTH);
        JScrollPane userScrollPane = new JScrollPane(userList);
        center.add(userScrollPane, BorderLayout.CENTER);

        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout(0, 5));
        contentPane.add(toolBar, BorderLayout.NORTH);
        contentPane.add(center, BorderLayout.CENTER);

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                //关闭时，退出软件，此时需发送一个退出广播，通知其他在线用户
                LogUtils.info("程序退出");
                MessageSender.sendLogoutBroadcast();
                Config.STATUS = Config.STATUS_SHUTDOWN;
                System.exit(0);
            }
        });
        this.setTitle("局域网聊天工具");
        this.setIconImage(UIUtils.getIconImage("images/app64.png"));
        this.setSize(new Dimension(Config.MAIN_FRAME_WIDTH, Config.MAIN_FRAME_HEIGHT));
        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == refresh) {
            refresh();
        }
    }

    private synchronized void refresh() {
        users.clear();
        users.add(self);
        userList.setListData(new Vector<>(users));
        label.setText(UIUtils.getLabelString(users.size()));
        MessageSender.sendLoginBroadcast();
    }

    /**
     * 判断和hostAddress指定的ip地址的聊天框是否存在
     */
    public boolean hasTalkFrame(String hostAddress) {
        for (TalkFrame talkFrame : this.talkFrames) {
            if (talkFrame.getHostAddress().equals(hostAddress))
                return true;
        }
        return false;
    }

    public synchronized TalkFrame getTalkFrameSync(User user) {
        TalkFrame tf = getTalkFrame(user);
        if (tf == null) {
            tf = new TalkFrame(user);
            this.addTalkFrame(tf);
        }
        return tf;
    }

    /**
     * 根据hostAddress返回对应的聊天框
     */
    public TalkFrame getTalkFrame(User user) {
        for (TalkFrame tf : this.talkFrames) {
            if (tf.getHostAddress().equals(user.getHostAddress()))
                return tf;
        }
        return null;
    }

    public void addTalkFrame(TalkFrame tf) {
        this.talkFrames.add(tf);
    }

    /**
     * 收到用户登陆广播消息时更新在线用户列表,如果用户已存在列表中则忽略
     */
    public synchronized void addUser(User user) {
        LogUtils.info("加入一个用户：%s", user.toString());
        for (User u : users) {
            if (u.getHostAddress().equals(user.getHostAddress())) {
                LogUtils.info("该用户已存在：%s", user.toString());
                return;
            }
        }
        users.add(user);
        userList.setListData(new Vector<>(users));
        label.setText(UIUtils.getLabelString(users.size()));
    }

    /**
     * 收到用户退出广播消息时更新在线用户列表
     */
    public synchronized void removeUser(User user) {
        for (User u : this.users) {
            if (u.getHostAddress().equals(user.getHostAddress())) {
                users.remove(u);
                break;
            }
        }
        userList.setListData(new Vector<>(users));
        label.setText(UIUtils.getLabelString(users.size()));
    }

}

class MyListCellRenderer extends JLabel implements ListCellRenderer<User> {

    private static final long serialVersionUID = 1L;
    private final ImageIcon userIcon = UIUtils.getImageIcon("images/user_48.png");
    private Border unselectedBorder = null;
    private Border selectedBorder = null;

    MyListCellRenderer() {
        setOpaque(true);
    }

    @Override
    public Component getListCellRendererComponent(JList list, User user,
                                                  int index, boolean isSelected, boolean cellHasFocus) {
        this.setLayout(new BorderLayout());
        setText("<html><font color='blue'>" + user.getUserName() + "(" + user.getHostName() + ")</font><br/><font color='#A9A9A9'>" +
                user.getHostAddress() + "</font></html>");
        setIcon(userIcon);
        setToolTipText("双击开始聊天");
        if (isSelected) {
            if (selectedBorder == null) {
                selectedBorder = BorderFactory.createMatteBorder(1, 1, 1, 1, Color.blue);
            }
            setBorder(selectedBorder);
            setBackground(Config.SYS_COLOR_SELECTED);
        } else {
            if (unselectedBorder == null) {
                unselectedBorder = BorderFactory.createMatteBorder(1, 1, 1, 1, Color.gray);
            }
            setBorder(unselectedBorder);
            setBackground(Color.white);
        }
        setEnabled(list.isEnabled());
        setFont(list.getFont());
        return this;
    }

}