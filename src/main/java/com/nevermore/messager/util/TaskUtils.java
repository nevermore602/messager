package com.nevermore.messager.util;

import com.google.common.util.concurrent.*;
import com.nevermore.messager.bean.MessageInfo;
import com.nevermore.messager.bean.TbMsg;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.*;

public class TaskUtils {
    private static final ThreadFactory taskExecutorThreadFactory = new ThreadFactoryBuilder()
            .setNameFormat("task-executor-%d").setDaemon(true)
            .build();

    private static final ListeningExecutorService executorService =
            MoreExecutors.listeningDecorator(new ThreadPoolExecutor(100, 100,
                    10L, TimeUnit.MILLISECONDS,
                    new LinkedBlockingQueue<>(300),
                    taskExecutorThreadFactory));

    private static final BlockingQueue<MessageInfo> talkMessageQueue = new LinkedBlockingQueue<>(); // 聊天消息队列
    private static final BlockingQueue<MessageInfo> otherMessageQueue = new LinkedBlockingQueue<>(); // 其他消息队列
    private static final BlockingQueue<TbMsg> dbWriteQueue = new LinkedBlockingQueue<>(); // 其他消息队列

    public static void putTalkMessage(MessageInfo message) throws InterruptedException {
        talkMessageQueue.put(message);
    }

    public static MessageInfo takeTalkMessage() throws InterruptedException {
        return talkMessageQueue.take();
    }

    public static void putOtherMessage(MessageInfo message) throws InterruptedException {
        otherMessageQueue.put(message);
    }

    public static MessageInfo takeOtherMessage() throws InterruptedException {
        return otherMessageQueue.take();
    }

    public static void putDbWriteTbMsg(TbMsg message) throws InterruptedException {
        dbWriteQueue.put(message);
    }

    public static TbMsg takeDbWriteTbMsg() throws InterruptedException {
        return dbWriteQueue.take();
    }

    // 一次获取多个数据（如果有），由于drainTo()方法不是阻塞式的，因此通过take()方法来配合完成阻塞式一次性获取多个
    public static Collection<TbMsg> takeMultiDbWriteTbMsgs(int maxSize) throws InterruptedException {
        Collection<TbMsg> collection = new ArrayList<>(maxSize);
        TbMsg tbMsg = dbWriteQueue.take();
        collection.add(tbMsg);
        dbWriteQueue.drainTo(collection, maxSize - 1); // 此方法一次性获取多个数据，但是不是阻塞式的
        return collection;
    }

    public static <T> void executeWithCallback(Callable<T> task, FutureCallback<T> callback) {
        Futures.addCallback(executorService.submit(task), callback);
    }

    public static void executeWithCallback(Runnable task, FutureCallback<Object> callback) {
        Futures.addCallback(executorService.submit(task), callback);
    }

    public static void execute(Runnable task) {
        executorService.execute(task);
    }

    public static void main(String[] args) {
        TaskUtils.executeWithCallback(
                (Runnable) () -> {
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    throw new RuntimeException("test");
                },
                new FutureCallback<Object>() {
                    @Override
                    public void onSuccess(Object result) {
                        LogUtils.info("onSuccess");
                        LogUtils.info(result.toString());
                    }

                    @Override

                    public void onFailure(Throwable t) {
                        LogUtils.info("onFailure");
                        t.printStackTrace();
                    }
                }
        );
    }

}
