package com.nevermore.messager.util;


import java.util.Map;

public class SysUtils {
    /**
     * 获取本机上当前的登录用户
     */
    public static String getLoginUserName() {
        Map<String, String> map = System.getenv();
        return map.getOrDefault("USERNAME",
                map.getOrDefault("USER",
                        map.getOrDefault("LOGNAME", "unknown")));
    }

}