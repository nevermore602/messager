package com.nevermore.messager.util;

import com.nevermore.messager.bean.TbMsg;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.Collection;


public class DBUtils {
    public static final String BASE_PATH = "";
    private Connection conn;
    private PreparedStatement ps;
    private ResultSet rs;

    public DBUtils(String dbPath) throws SQLException, ClassNotFoundException {
        String path = BASE_PATH + dbPath;
        Class.forName("org.sqlite.JDBC");
        conn = DriverManager.getConnection("jdbc:sqlite:" + path, null, null);
    }

    /**
     * 插入一条通讯信息的工具方法
     * 如果只开启一条写库线程，则此方法不需要做同步，否则，需要同步（不然在高并发下会导致多线程同时写库失败）
     */
    public static void insertMsg(TbMsg msg) throws SQLException, ClassNotFoundException {
        DBUtils dbUtil = new DBUtils("data/userdata.db");
        dbUtil.insert(msg);
        dbUtil.close();
    }

    /**
     * 批量插入通讯信息的工具方法
     * 如果只开启一条写库线程，则此方法不需要做同步，否则，需要同步（不然在高并发下会导致多线程同时写库失败）
     */
    public static void batchInsertMsg(Collection<TbMsg> msgs) throws SQLException, ClassNotFoundException {
        DBUtils dbUtil = new DBUtils("data/userdata.db");
        dbUtil.batchInsert(msgs);
        dbUtil.close();
    }

    public static void checkMsgDB() throws IOException, SQLException, ClassNotFoundException {
        File dataFolder = new File(BASE_PATH + "data");
        if (!dataFolder.exists() || !dataFolder.isDirectory()) {
            dataFolder.mkdir();
        }
        File dbFile = new File(BASE_PATH + "data/userdata.db");
        if (!dbFile.exists() || !dbFile.isFile()) { // 如果不存在或者存在但不是文件
            LogUtils.info("开始创建数据库...");
            if (!dbFile.getParentFile().exists()) {
                dbFile.getParentFile().mkdirs();
            }
            dbFile.createNewFile();
            DBUtils dbUtil = new DBUtils("data/userdata.db");
            String sql = "CREATE TABLE  tb_msg  ("
                    + "id  INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + " userName  VARCHAR2(30) NOT NULL, hostName  VARCHAR2(30) NOT NULL, "
                    + "macAddress  CHAR(17) NOT NULL, hostAddress  VARCHAR2(15) NOT NULL, "
                    + "localUserName  VARCHAR2(30) NOT NULL, "
                    + "localHostName  VARCHAR2(30) NOT NULL, localHostAddress  VARCHAR2(15) NOT NULL, "
                    + " localMacAddress  CHAR(17) NOT NULL,  type  CHAR(1) NOT NULL, "
                    + " messageType  CHAR(2) NOT NULL,  messageText  VARCHAR2(512), "
                    + " sendTime  VARCHAR2(19) NOT NULL,  receiveTime  VARCHAR2(19) NOT NULL);";
            Connection conn = dbUtil.getConnection();
            Statement st = conn.createStatement();
            st.executeUpdate(sql);
            dbUtil.close();
            LogUtils.info("数据库创建成功！");
        }
    }

    public Connection getConnection() {
        return conn;
    }

    private void batchInsert(Collection<TbMsg> msgs) {
        String insertSql = "insert into tb_msg (userName,hostName,macAddress,hostAddress,localUserName,localHostName,"
                + " localHostAddress,localMacAddress,type,messageType,messageText,sendTime,receiveTime)  "
                + " values (?,?,?,?,?,?,?,?,?,?,?,?,?)";
        try {
            ps = conn.prepareStatement(insertSql);
            conn.setAutoCommit(false);
            for (TbMsg msg : msgs) {
                String localMacAddr = msg.getLocalMacAddress();
                if (localMacAddr == null || "".equals(localMacAddr)) {
                    msg.setLocalMacAddress("unknown");
                }
                String macAddr = msg.getMacAddress();
                if (macAddr == null || "".equals(macAddr)) {
                    msg.setMacAddress("unknown");
                }
                ps.setString(1, msg.getUserName());
                ps.setString(2, msg.getHostName());
                ps.setString(3, msg.getMacAddress());
                ps.setString(4, msg.getHostAddress());
                ps.setString(5, msg.getLocalUserName());
                ps.setString(6, msg.getLocalHostName());
                ps.setString(7, msg.getLocalHostAddress());
                ps.setString(8, msg.getLocalMacAddress());
                ps.setString(9, msg.getType());
                ps.setString(10, msg.getMessageType());
                ps.setString(11, msg.getMessageText());
                ps.setString(12, msg.getSendTime());
                ps.setString(13, msg.getReceiveTime());
                ps.addBatch();
            }
            ps.executeBatch();
            conn.commit();
        } catch (SQLException e) {
            LogUtils.warn("批量写入消息数据出现异常！", e);
        }
    }

    private void insert(TbMsg msg) {
        String localMacAddr = msg.getLocalMacAddress();
        if (localMacAddr == null || "".equals(localMacAddr)) {
            msg.setLocalMacAddress("unknown");
        }

        String macAddr = msg.getMacAddress();
        if (macAddr == null || "".equals(macAddr)) {
            msg.setMacAddress("unknown");
        }
        try {
            String insertSql = "insert into tb_msg (userName,hostName,macAddress,hostAddress,localUserName,localHostName,"
                    + " localHostAddress,localMacAddress,type,messageType,messageText,sendTime,receiveTime)  "
                    + " values (?,?,?,?,?,?,?,?,?,?,?,?,?)";
            ps = conn.prepareStatement(insertSql);
            ps.setString(1, msg.getUserName());
            ps.setString(2, msg.getHostName());
            ps.setString(3, msg.getMacAddress());
            ps.setString(4, msg.getHostAddress());
            ps.setString(5, msg.getLocalUserName());
            ps.setString(6, msg.getLocalHostName());
            ps.setString(7, msg.getLocalHostAddress());
            ps.setString(8, msg.getLocalMacAddress());
            ps.setString(9, msg.getType());
            ps.setString(10, msg.getMessageType());
            ps.setString(11, msg.getMessageText());
            ps.setString(12, msg.getSendTime());
            ps.setString(13, msg.getReceiveTime());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                if (ps != null)
                    try {
                        ps.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            conn.close();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
            }
        }
    }

}