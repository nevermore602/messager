package com.nevermore.messager.util;

import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.plaf.FontUIResource;
import java.awt.*;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Objects;

public class UIUtils {

    /**
     * 用于显示错误信息的对话框
     */
    public static void showErroMessage(Component f, String msg) {
        JOptionPane.showMessageDialog(f, msg, "错误", JOptionPane.ERROR_MESSAGE);
    }

    /**
     * 用于显示警告信息的对话框
     */
    public static void showAlertMessage(Component f, String msg) {
        JOptionPane.showMessageDialog(f, msg, "提示", JOptionPane.WARNING_MESSAGE);
    }

    /**
     * 初始化系统的默认字体
     */
    public static void initGlobalFont(Font fnt) {
        FontUIResource fontRes = new FontUIResource(fnt);
        for (Enumeration<Object> keys = UIManager.getDefaults().keys(); keys.hasMoreElements(); ) {
            Object key = keys.nextElement();
            Object value = UIManager.get(key);
            if (value instanceof FontUIResource) {
                UIManager.put(key, fontRes);
            }
        }
    }


    /**
     * 将窗体放置在屏幕中间
     */
    public static void setLocationCenter(JFrame f) {
        f.setLocationRelativeTo(null);
    }


    public static String getLabelString(int num) {
        return "在线用户数：" + num + "人";
    }

    /**
     * 得到资源图片
     */
    public static ImageIcon getImageIcon(String imgFile) {
        return new ImageIcon(Objects.requireNonNull(UIUtils.class.getClassLoader().getResource(imgFile)));
    }

    public static Image getIconImage(String iconFile) {
        try {
            return ImageIO.read(UIUtils.class.getClassLoader().getResourceAsStream(iconFile));
        } catch (IOException e) {
            LogUtils.warn("读取资源文件出错！", e);
            return null;
        }
    }

    /**
     * 得到动态图片
     */
    public static ImageIcon getImageIconFromGIF(String gifFile) {
        try {
            Image image = ImageIO.read(UIUtils.class.getClassLoader().getResourceAsStream(gifFile));
            return new ImageIcon(image);
        } catch (IOException ex) {
            LogUtils.warn("读取资源文件出错！", ex);
            return null;
        }
    }

    /**
     * 播放au格式音效
     */
    public static void playSound(String soundPath) {
        AudioStream as = null;
        try {
            as = new AudioStream(UIUtils.class.getClassLoader().getResourceAsStream(soundPath));
        } catch (IOException e) {
            LogUtils.warn("加载音频资源出错！", e);
        }
        AudioPlayer.player.start(as);
    }

    /**
     * 根据16进制颜色值得到Color对象
     *
     * @param color 格式为‘#FFFFFF’或‘FFFFFF’
     * @return Color对象
     */
    static public Color getColorFromHex(String color) {
        if (color.charAt(0) == '#') {
            color = color.substring(1);
        }
        if (color.length() != 6) {
            return null;
        }
        try {
            int r = Integer.parseInt(color.substring(0, 2), 16);
            int g = Integer.parseInt(color.substring(2, 4), 16);
            int b = Integer.parseInt(color.substring(4), 16);
            return new Color(r, g, b);
        } catch (NumberFormatException nfe) {
            return null;
        }
    }

}