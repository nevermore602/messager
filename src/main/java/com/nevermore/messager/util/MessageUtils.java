package com.nevermore.messager.util;

import com.google.common.base.Charsets;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.primitives.Ints;
import com.google.common.primitives.Longs;
import com.google.common.primitives.Shorts;
import com.nevermore.messager.bean.Message;
import com.nevermore.messager.bean.MessageInfo;
import com.nevermore.messager.bean.TbMsg;
import com.nevermore.messager.bean.User;
import com.nevermore.messager.handler.MessageSender;
import com.nevermore.messager.ui.MainFrame;
import com.nevermore.messager.ui.TalkFrame;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class MessageUtils {
    // 缓存需要合包的消息 key: [host]-[seq]
    private static Cache<String, Message[]> subMsgCache = CacheBuilder.newBuilder()
            .maximumSize(Long.MAX_VALUE)
            .expireAfterAccess(30, TimeUnit.SECONDS)
            .build();

    private synchronized static void putSubMsg(String key, Message subMsg) {
        Message[] messages = subMsgCache.getIfPresent(key);
        if (messages == null) {
            messages = new Message[subMsg.getSubNum()];
            Arrays.fill(messages, null); // 先全部用null填充
        }
        messages[subMsg.getSubSeq()] = subMsg;
        subMsgCache.put(key, messages);
    }

    // 判断是否所有分包都已收到
    private synchronized static boolean isAllSubMsgReceived(String key) {
        Message[] messages = subMsgCache.getIfPresent(key);
        if (messages == null || messages.length == 0) {
            return false;
        }
        for (Message message : messages) {
            if (message == null) {
                return false;
            }
        }
        return true;
    }

    private synchronized static Message mergeSubMsgs(String key) {
        Message[] messages = subMsgCache.getIfPresent(key);
        if (messages == null || messages.length == 0) {
            throw new RuntimeException("不存在分包，无法合包");
        }
        Message mergedMsg = new Message();
        List<Byte> dataBytes = new ArrayList<>();
        for (int i = 0; i < messages.length; i++) {
            Message msg = messages[i];
            if (msg == null) {
                throw new RuntimeException(String.format("分包[index=%s]未收到，无法合包", i));
            }
            for (int k = 0; k < msg.getData().length; k++) {
                dataBytes.add(msg.getData()[k]);
            }
        }
        byte[] data = new byte[dataBytes.size()];
        for (int i = 0; i < dataBytes.size(); i++) {
            data[i] = dataBytes.get(i);
        }
        Message first = messages[0];
        mergedMsg.setData(data);
        mergedMsg.setSeq(first.getSeq());
        mergedMsg.setCmd(first.getCmd());
        mergedMsg.setSubNum((byte) 1);
        mergedMsg.setSubSeq((byte) 0);
        mergedMsg.setHostName(first.getHostName());
        mergedMsg.setUserName(first.getUserName());

        subMsgCache.invalidate(key); // 合并完移除
        LogUtils.receiveLog("合并的消息：%s", mergedMsg.toString());
        return mergedMsg;
    }

    public static Message parseMessage(byte msgBytes[], int length) {
        byte[] seqBytes = Arrays.copyOfRange(msgBytes, 0, 8);
        byte headByte = msgBytes[8];
        byte subNumByte = msgBytes[9];
        byte subSeqByte = msgBytes[10];
        byte[] dataLengthBytes = Arrays.copyOfRange(msgBytes, 11, 13);
        int dataLength = Shorts.fromByteArray(dataLengthBytes);
        byte[] data = Arrays.copyOfRange(msgBytes, 13, 13 + dataLength);
        byte[] otherBytes = Arrays.copyOfRange(msgBytes, 13 + dataLength, length);
        String otherStr = new String(otherBytes, Charsets.UTF_8);
        String[] otherStrs = otherStr.split(":");
        Message msg = new Message();
        msg.setSeq(Longs.fromByteArray(seqBytes));
        msg.setCmd(headByte);
        msg.setSubNum(subNumByte);
        msg.setSubSeq(subSeqByte);
        msg.setUserName(otherStrs[0]);
        msg.setHostName(otherStrs[1]);
        msg.setData(data);
        return msg;
    }

    public static void handleTalkMessage(MessageInfo messageInfo) {
        final Message msg = messageInfo.getMessage();
        final String hostAddress = messageInfo.getHostAddr();
        String msgData = msg.getMessage();
        if (msg.getSubNum() > (byte) 1) { // 如果是分包
            String key = hostAddress + "-" + msg.getSeq();
            putSubMsg(key, msg);
            if (isAllSubMsgReceived(key)) {
                Message mergedMsg = mergeSubMsgs(key);
                handleTalkMessage(MessageInfo.builder().hostAddr(hostAddress).message(mergedMsg).build());
            }
            return;
        }

        User user = new User();
        user.setHostAddress(hostAddress);
        user.setHostName(msg.getHostName());
        user.setUserName(msg.getUserName());

        String sendTime = BasicUtils.formatDate(new Date(msg.getSeq()));
        String info = msg.getUserName() + " " + sendTime;
        // 如果接收到的是聊天消息
        TalkFrame tf = MainFrame.getInstance().getTalkFrameSync(user);

        if (!tf.isVisible()) {
            tf.setVisible(true);
        }
        tf.appendMsg(1, info, msgData);
        UIUtils.playSound("sounds/msg.wav");
        TaskUtils.execute(() -> {
            try {
                TaskUtils.putDbWriteTbMsg(buildReceiveTbMsg(msg, hostAddress));
            } catch (InterruptedException e) {
                LogUtils.warn("接收消息的写库消息入队列失败！", e);
            }
        });
    }

    public static void handleOtherMessage(MessageInfo messageInfo) {
        String hostAddress = messageInfo.getHostAddr();
        Message msg = messageInfo.getMessage();
        byte msgHead = msg.getCmd();
        String msgData = msg.getMessage();
        if (msg.getSubNum() > (byte) 1) { // 如果是分包
            String key = hostAddress + "-" + msg.getSeq();
            putSubMsg(key, msg);
            if (isAllSubMsgReceived(key)) {
                Message mergedMsg = mergeSubMsgs(key);
                handleOtherMessage(MessageInfo.builder().hostAddr(hostAddress).message(mergedMsg).build());
            }
            return;
        }

        User user = new User();
        user.setHostAddress(hostAddress);
        user.setHostName(msg.getHostName());
        user.setUserName(msg.getUserName());

        //String sendTime = BasicUtils.formatDate(new Date(msg.getSeq()));
        boolean isSelf = Config.LOCAL_HOST_ADDR.equals(hostAddress);
        if (msgHead == Config.CMD_LOGIN_MSG) { // 登陆时发送的广播消息
            receiveLoginMessage(user);
        } else if (msgHead == Config.CMD_LOGIN_REPLY_MSG) { // 回复登陆广播的消息，不用考虑是否为本机发送，因为本机收不到自身的登陆包
            receiveLoginReplyMessage(user);
        } else if (msgHead == Config.CMD_LOGOUT_MSG && !isSelf) { // 退出时发送的广播消息
            receiveLogoutMessage(user);
        } else if (msgHead == Config.CMD_REQUEST_SEND_FILE) { //请求传送文件的消息
            receiveSendFileMessage(user, msgData);
        } else if (msgHead == Config.CMD_SHAKE) { //窗口抖动消息
            receiveShakeMessage(user);
        } else if (msgHead == Config.CMD_REFUSE_FILE) { //拒绝接收文件的消息
            receiveRefuseFileMessage(user);
        } else if (msgHead == Config.CMD_CANCEL_RECEIVE_FILE) { //取消接收文件的消息
            receiveCancelReceiveFileMessage(user);
        } else if (msgHead == Config.CMD_CANCEL_SEND_FILE) { //取消发送文件的消息
            receiveCancelSendFileMessage(user);
        } else if (msgHead == Config.CMD_REQUEST_VOICE) { //请求语音聊天
            receiveRequestVoiceMessage(user);
        } else if (msgHead == Config.CMD_ACCEPT_VOICE) { //接受语音聊天
            receiveAcceptVoiceMessage(user);
        } else if (msgHead == Config.CMD_REFUSE_VOICE) {//拒绝语音聊天
            receiveRefuseVoiceMessage(user);
        } else if (msgHead == Config.CMD_STOP_VOICE) { //中断语音聊天
            receiveStopVoiceMessage(user);
        }
    }

    private static void receiveCancelSendFileMessage(User user) {
        TalkFrame tf = MainFrame.getInstance().getTalkFrameSync(user);
        tf.appendSysMsg(Config.CANCEL_SEND_FILE_MSG_TIP);
        tf.cancelReceiveFile();
    }

    private static void receiveCancelReceiveFileMessage(User user) {
        TalkFrame tf = MainFrame.getInstance().getTalkFrameSync(user);
        tf.appendSysMsg(Config.CANCEL_RECEIVE_FILE_MSG_TIP);
        tf.cancelSendFile();
    }

    private static void receiveStopVoiceMessage(User user) {
        TalkFrame tf = MainFrame.getInstance().getTalkFrameSync(user);
        tf.setVisible(true);
        tf.appendSysMsg(Config.STOP_VOICE_TIP);
        tf.stopVoice();
    }

    private static void receiveAcceptVoiceMessage(User user) {
        TalkFrame tf = MainFrame.getInstance().getTalkFrameSync(user);
        tf.setVisible(true);
        tf.appendSysMsg(Config.ACCEPT_VOICE_TIP);
        tf.startVoice();
    }


    private static void receiveRefuseVoiceMessage(User user) {
        TalkFrame tf = MainFrame.getInstance().getTalkFrameSync(user);
        tf.setVisible(true);
        tf.appendSysMsg(Config.REFUSE_VOICE_TIP);
    }


    private static void receiveRequestVoiceMessage(User user) {
        TalkFrame tf = MainFrame.getInstance().getTalkFrameSync(user);
        tf.setVisible(true);
        tf.appendSysMsg(Config.REQUEST_VOICE_TIP);
        tf.receiveRequestVoiceMsg();
    }


    private static void receiveRefuseFileMessage(User user) {
        TalkFrame tf = MainFrame.getInstance().getTalkFrameSync(user);
        tf.setVisible(true);
        tf.appendSysMsg(Config.REFUSE_FILE_MSG_TIP);
        tf.recoverSendPanel();
        tf.stopFileServer();
    }

    private static void receiveSendFileMessage(User user, String msgData) {
        String[] fields = msgData.split("\\*");
        String portStr = fields[0];
        String fileNames = fields[1];
        String fileLengths = fields[2];
        String[] names = fileNames.split("\\|");
        String[] lengthStrs = fileLengths.split("\\|");
        long[] lengths = new long[lengthStrs.length];
        for (int i = 0; i < lengthStrs.length; i++) {
            lengths[i] = Long.parseLong(lengthStrs[i]);
        }
        TalkFrame tf = MainFrame.getInstance().getTalkFrameSync(user);
        tf.setFilePort(Integer.parseInt(portStr));
        tf.setVisible(true);
        tf.appendSysMsg(Config.SEND_FILE_MSG_TIP);
        tf.getFilePanel().enableOperateBtn();
        tf.setFileNames(names);
        tf.setFileLengths(lengths);
        tf.updateReceiveTable();
        tf.openFilePanel();
    }

    private static void receiveLogoutMessage(User user) {
        MainFrame.getInstance().removeUser(user);
    }

    private static void receiveShakeMessage(User user) {
        TalkFrame tf = MainFrame.getInstance().getTalkFrameSync(user);
        tf.setVisible(true);
        tf.appendSysMsg(Config.SHAKE_TIP);
        tf.shake();
    }

    private static void receiveLoginReplyMessage(User user) {
        MainFrame.getInstance().addUser(user);
    }

    private static void receiveLoginMessage(User user) {
        if (!Config.LOCAL_HOST_ADDR.equals(user.getHostAddress())) { // 如果非自身，则回复
            // 收到登陆包后需发送一个登陆回应包，告诉对方自己在线
            MessageSender.sendLoginReplyMsg(user.getHostAddress());
            MainFrame.getInstance().addUser(user);
        }
    }

    /**
     * 构建接收到的消息（只写聊天信息和文件传送的记录）
     *
     * @param m        消息对象
     * @param hostAddr 对方IP地址
     */
    private static TbMsg buildReceiveTbMsg(Message m, String hostAddr) {
        TbMsg msg = new TbMsg();
        msg.setHostAddress(hostAddr);
        msg.setHostName(m.getHostName());
        msg.setUserName(m.getUserName());
        msg.setLocalHostAddress(Config.LOCAL_HOST_ADDR);
        msg.setLocalHostName(Config.LOCAL_HOST_NAME);
        msg.setLocalMacAddress(Config.LOCAL_MAC_ADDR);
        msg.setLocalUserName(Config.LOCAL_USER_NAME_DEFAULT);
        msg.setType("1"); //0表示发送的消息；1表示接收的消息
        msg.setMacAddress("Unknown");  //暂时没做，预留
        msg.setMessageText(m.getMessage());
        msg.setMessageType(String.valueOf(m.getCmd()));
        msg.setSendTime(BasicUtils.formatDate(new Date(m.getSeq()), false));
        msg.setReceiveTime(BasicUtils.formatDate(new Date(), false));
        return msg;
    }

    /**
     * 构建发送的消息（只写聊天信息和文件传送记录）
     *
     * @param m    消息对象
     * @param user 对方user对象
     */
    public static TbMsg buildSendTbMsg(Message m, User user) {
        TbMsg msg = new TbMsg();
        msg.setHostAddress(user.getHostAddress());
        msg.setHostName(user.getHostName());   //通过查找该用户信息得到
        msg.setUserName(user.getUserName());   //通过查找该用户信息得到
        msg.setLocalHostAddress(Config.LOCAL_HOST_ADDR);
        msg.setLocalHostName(m.getHostName());
        msg.setLocalMacAddress(Config.LOCAL_MAC_ADDR);
        msg.setLocalUserName(m.getUserName());
        msg.setType("0"); //0表示发送的消息；1表示接收的消息
        msg.setMacAddress(user.getMacAddress());  //对方的Mac地址，通过查找该用户信息得到
        msg.setMessageText(m.getMessage());
        msg.setMessageType(String.valueOf(m.getCmd()));
        msg.setSendTime(BasicUtils.formatDate(new Date(m.getSeq()), false));
        msg.setReceiveTime("Unknown"); //发送信息时暂时是不知道对方接收时间的，以后可能通过对方确认消息得到，暂时没做，预留
        return msg;
    }
}
