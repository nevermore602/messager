package com.nevermore.messager.util;

import com.google.common.primitives.Longs;
import com.google.common.primitives.Shorts;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class BasicUtils {
    private static DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
    private static DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * @param date     要格式化的日期
     * @param timeOnly 是则只保留时间部分舍弃日期部分，否则为日期+时间
     */
    public static String formatDate(Date date, boolean timeOnly) {
        if (timeOnly) {
            return timeFormat.format(date);
        } else {
            return dateTimeFormat.format(date);
        }
    }

    public static String formatDate(Date date) {
        return formatDate(date, true);
    }

    public static String convertByte(long byteData) {
        String result = byteData + "B";
        double temp;
        if (byteData >= 1024) {
            temp = byteData / 1024;
            result = String.format("%.1f", temp) + "KB";
            if (temp >= 1024) {
                temp = temp / 1024;
                result = String.format("%.1f", temp) + "MB";
                if (temp >= 1024) {
                    temp = temp / 1024;
                    result = String.format("%.2f", temp) + "GB";
                }
            }
        }
        return result;
    }

    public static String convertTime(long second) {
        String result = second + "秒";
        long m;
        long h;
        long s;
        if (second >= 60) {
            m = second / 60;
            s = second % 60;
            result = m + "分" + s + "秒";
            if (m >= 60) {
                h = m / 60;
                m = m % 60;
                result = h + "小时" + m + "分" + s + "秒";
            }
        }
        return result;
    }

    public static void main(String[] args) {
        short lg = 1187;
        System.out.println(Arrays.toString(Longs.toByteArray((long) lg)));
        System.out.println(Arrays.toString(Shorts.toByteArray(lg)));
    }
}