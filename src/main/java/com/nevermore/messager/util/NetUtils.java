package com.nevermore.messager.util;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

public class NetUtils {

    /**
     * 获取真实的本机信息，返回数据格式：mac地址;IP地址;主机名
     */
    public static String getLocalHostInfo() throws SocketException {
        String macStr = "";
        String ipStr = "";
        String nameStr = "";

        Enumeration<NetworkInterface> netInterfaces = NetworkInterface.getNetworkInterfaces();
        while (netInterfaces.hasMoreElements()) {
            NetworkInterface netInterface = netInterfaces.nextElement();
            // 去除回环接口，子接口，未运行的接口
            if (netInterface.isLoopback() || netInterface.isVirtual() || !netInterface.isUp()) {
                LogUtils.info("排除回环接口，子接口，未运行的接口：%s , %s", netInterface.getName(), netInterface.getDisplayName());
                continue;
            }
            String displayName = netInterface.getDisplayName();
            if (displayName.contains("Virtual")) {
                LogUtils.info("排除名称中包含Virtual的接口：%s , %s", netInterface.getName(), netInterface.getDisplayName());
                continue;
            }
            Enumeration<InetAddress> addresses = netInterface.getInetAddresses();
            while (addresses.hasMoreElements()) {
                InetAddress ip = addresses.nextElement();
                if (ip != null) {
                    if (ip instanceof Inet4Address) { // 仅支持ipv4
                        ipStr = ip.getHostAddress();
                        nameStr = ip.getHostName();
                        macStr = acquireMacAddr(netInterface);
                        break;
                    }
                }
            }
            if (ipStr.length() > 0) { // 找到一个就退出
                break;
            }
        }

        return macStr + ";" + ipStr + ";" + nameStr;
    }

    private static String acquireMacAddr(NetworkInterface ni) throws SocketException {
        StringBuilder sb = new StringBuilder();
        byte[] macs = ni.getHardwareAddress(); //硬件地址的字节数组
        if (macs == null) {
            return "";
        }
        //下面循环将字节数组每一个元素（byte表示的数字）转换为该数值的16进制表示
        for (byte mac : macs) {
            String macStr = Integer.toHexString(mac & 0xFF);
            if (macStr.length() == 1) {
                macStr = '0' + macStr;
            }
            sb.append(macStr).append("-");
        }
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1);
        }
        return sb.toString();
    }

    public static void main(String[] args) throws SocketException {
        getLocalHostInfo();
    }
}