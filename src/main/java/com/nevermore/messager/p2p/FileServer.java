package com.nevermore.messager.p2p;

import com.nevermore.messager.ui.TalkFrame;
import com.nevermore.messager.util.*;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class FileServer extends Thread {
    private static final String TAG = "FILE SERVER INFO: ";
    private TalkFrame parentFrame;
    private ServerSocket serverSocket;
    private Socket socket;
    private BufferedOutputStream socketOS;
    private BufferedInputStream socketIS;
    private String[] fileNames;
    private long[] fileLengths;
    private long progress;
    private boolean isCanceled = false;

    public FileServer(TalkFrame parentFrame, String[] fileNames, long[] fileLengths) throws IOException {
        this.parentFrame = parentFrame;
        this.fileLengths = fileLengths;
        this.fileNames = fileNames;
        this.progress = 0;
        serverSocket = new ServerSocket(0); // 0 自动分配端口
        LogUtils.sendLog(TAG + "服务器已经启动！绑定的端口为：%d", serverSocket.getLocalPort());
    }

    public void run() {
        if (fileLengths.length == 0 || fileNames.length == 0) {
            return;
        }
        try {
            socket = serverSocket.accept();
            LogUtils.sendLog(TAG + "对方连接成功！");
            socketOS = new BufferedOutputStream(socket.getOutputStream());
            socketIS = new BufferedInputStream(socket.getInputStream());
            int len; // 读入缓存字节数组的数据长度
            byte buffer[] = new byte[1024];
            long totalCostTime = 0; //总耗时
            int totalFileCount = 0; //总文件数目
            long totalFileLength = 0; //总文件大小

            while (!isCanceled && ((len = socketIS.read(buffer)) != -1)) {
                String msg = new String(buffer, 0, len);
                int index = Integer.parseInt(msg);
                LogUtils.sendLog(TAG + "对方请求的文件序号是：" + index);
                //当传送完最后一个文件会再发送一个整数信息，其值等于文件个数，表明已全部接收完毕，则关闭服务器
                if (index == -1) {
                    LogUtils.sendLog(TAG + "文件传送完毕，退出循环");
                    break;
                }
                LogUtils.sendLog(TAG + "============================================================");
                LogUtils.sendLog(TAG + "开始传送第【%s】号文件[%s]", index, fileNames[index]);
                long startTime = System.currentTimeMillis();
                this.parentFrame.updateSendTableState(index, Config.STATE_SENDING);
                File fileToSend = new File(fileNames[index]);
                BufferedInputStream bis = new BufferedInputStream(new FileInputStream(fileToSend));
                this.progress = 0;
                long fileLength = fileLengths[index];
                while ((len = bis.read(buffer)) != -1) {
                    if (!isCanceled) {
                        socketOS.write(buffer, 0, len);
                        //进度信息
                        progress += len;
                        String lengthShow = BasicUtils.convertByte(fileLength);
                        String info = "(" + (index + 1) + "/" + fileNames.length + ")  " + BasicUtils.convertByte(progress) + "/" + lengthShow + "  " + progress * 100 / fileLength + "%";
                        int pValue = (int) (progress * 100 / fileLength);
                        parentFrame.updateSendProgress(pValue, info, "正在发送：" + fileNames[index]);
                    } else {
                        parentFrame.updateReceiveProgress((int) (progress * 100 / fileLength), "正在取消", "正在取消");
                        break;
                    }
                }
                socketOS.flush();
                IOUtils.closeQuietly(bis);
                if (!isCanceled) {
                    LogUtils.sendLog(TAG + "第【%s】号文件[%s]传送完毕～", index, fileNames[index]);
                    long costTime = (System.currentTimeMillis() - startTime);
                    totalCostTime += costTime;
                    totalFileCount++;
                    totalFileLength += fileLengths[index];
                    parentFrame.updateSendTableState(index, Config.STATE_COMPLETED);
                    parentFrame.appendSysMsg("文件[" + fileNames[index] + "]发送完毕，文件大小[" + BasicUtils.convertByte(fileLengths[index]) + "]，耗时["
                            + BasicUtils.convertTime(costTime / 1000) + "] ");
                } else {
                    break;
                }
            }
            if (!isCanceled) {
                parentFrame.appendSysMsg("所有文件发送完毕，文件总数目[" + totalFileCount + "]个，文件总大小[" + BasicUtils.convertByte(totalFileLength)
                        + "]，总耗时[" + BasicUtils.convertTime(totalCostTime / 1000) + "]");
            }
            parentFrame.recoverSendPanel();
        } catch (IOException e) {
            LogUtils.error("文件服务端出现异常！", e);
        } finally {
            TaskUtils.execute(() -> {
                try {
                    Thread.sleep(1500);
                } catch (InterruptedException e) {
                    // ignore
                }
                IOUtils.closeQuietly(socketOS);
                IOUtils.closeQuietly(socketIS);
                IOUtils.closeQuietly(socket);
                IOUtils.closeQuietly(serverSocket);
            });

        }
    }

    public void stopServer() {
        IOUtils.closeQuietly(serverSocket);
    }

    public void cancelSendFile() {
        isCanceled = true;
        stopServer();
    }

    public int getPort() {
        if (serverSocket != null && serverSocket.isBound()) {
            return serverSocket.getLocalPort();
        } else {
            throw new RuntimeException("Server socket is null or unbound!");
        }
    }
}