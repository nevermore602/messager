package com.nevermore.messager.p2p;

import com.nevermore.messager.ui.TalkFrame;
import com.nevermore.messager.util.*;

import java.io.*;
import java.net.Socket;
import java.util.Arrays;

public class FileClient extends Thread {
    private static final String TAG = "FILE CLIENT INFO: ";
    private Socket socket;
    private int port;
    private BufferedOutputStream socketOS;
    private BufferedInputStream socketIS;
    private TalkFrame talkFrame;
    private String hostAddress;
    private String savePath;
    private String[] fileNames;
    private long[] fileLengths;
    private boolean isCanceled = false;

    public FileClient(TalkFrame talkFrame, int port) {
        this.talkFrame = talkFrame;
        this.port = port;
        this.hostAddress = talkFrame.getHostAddress();
        this.fileLengths = talkFrame.getFileLengths();
        this.fileNames = talkFrame.getFileNames();
        this.savePath = talkFrame.getSavePath();
        LogUtils.receiveLog(TAG + Arrays.toString(this.fileNames));
        LogUtils.receiveLog(TAG + Arrays.toString(this.fileLengths));
    }

    public void run() {
        try {
            socket = new Socket(hostAddress, port);
            socketOS = new BufferedOutputStream(socket.getOutputStream());
            socketIS = new BufferedInputStream(socket.getInputStream());
            byte buffer[] = new byte[1024];
            int index = 0;
            long totalCostTime = 0; //总耗时
            int totalFileCount = 0; //总文件数目
            long totalFileLength = 0; //总文件大小
            while (index < fileNames.length && !isCanceled) {
                int len;
                long progress = 0;
                long fileLength = fileLengths[index];
                long startTime = System.currentTimeMillis();
                socketOS.write(String.valueOf(index).getBytes()); //先发送一条消息，通知服务器下一条要接收的文件编号
                socketOS.flush();
                this.talkFrame.updateReceiveTableState(index, Config.STATE_ACCEPTING);
                String tmpFilePath = this.savePath + File.separator + "messager_" + System.currentTimeMillis() + ".tmp";
                String realFilePath = this.savePath + File.separator + fileNames[index];
                File tmpFile = new File(tmpFilePath);
                BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(tmpFile));
                while (progress < fileLengths[index] && ((len = socketIS.read(buffer)) != -1)) {
                    if (!isCanceled) {
                        bos.write(buffer, 0, len);
                        //进度信息
                        progress += len;
                        String lengthShow = BasicUtils.convertByte(fileLength);
                        String info = "(" + (index + 1) + "/" + fileNames.length + ")  " + BasicUtils.convertByte(progress) + "/" + lengthShow + "  " + progress * 100 / fileLength + "%";
                        int pValue = (int) (progress * 100 / fileLength);
                        talkFrame.updateReceiveProgress(pValue, info, "正在接收：" + fileNames[index]);
                    } else {
                        talkFrame.updateReceiveProgress((int) (progress * 100 / fileLength), "正在取消", "正在取消");
                        break;
                    }
                }
                bos.flush();
                IOUtils.closeQuietly(bos);
                if (!isCanceled) {
                    tmpFile.renameTo(new File(realFilePath)); // 下载完成后重命名，如果文件已存在，会导致重命名失败，此处还需优化，在接收文件前先检测，让用户选择如何处理
                    LogUtils.receiveLog(TAG + "第【%s】号文件[%s]接收完毕～", index, fileNames[index]);
                    long costTime = (System.currentTimeMillis() - startTime);
                    totalCostTime += costTime;
                    totalFileCount++;
                    totalFileLength += fileLengths[index];
                    this.talkFrame.updateReceiveTableState(index, Config.STATE_COMPLETED);
                    talkFrame.appendSysMsg("文件[" + fileNames[index] + "]接收完毕，文件大小[" + BasicUtils.convertByte(fileLengths[index]) + "]，耗时["
                            + BasicUtils.convertTime(costTime / 1000) + "]");
                    index++;
                } else {
                    break;
                }
            }
            talkFrame.recoverReceiveTable();
            if (!isCanceled) {
                //最后全部接收完需要再发送一条消息
                socketOS.write(String.valueOf(-1).getBytes());
                socketOS.flush();
                talkFrame.appendSysMsg("所有文件接收完毕，文件总数目[" + totalFileCount + "]个，文件总大小[" + BasicUtils.convertByte(totalFileLength) + "]，总耗时[" + BasicUtils.convertTime(totalCostTime / 1000) + "]");
            }
            talkFrame.recoverReceivePanel();
        } catch (IOException e) {
            LogUtils.error("文件客户端出现异常！", e);
        } finally {
            TaskUtils.execute(() -> {
                try {
                    Thread.sleep(1500);
                } catch (InterruptedException e) {
                    // ignore
                }
                IOUtils.closeQuietly(socketIS);
                IOUtils.closeQuietly(socketOS);
                IOUtils.closeQuietly(socket);
            });
        }
    }

    public void cancelReceiveFile() {
        isCanceled = true;
    }
}