package com.nevermore.messager;

import com.nevermore.messager.ui.MainFrame;
import com.nevermore.messager.util.LogUtils;
import com.nevermore.messager.util.UIUtils;
import org.jvnet.substance.SubstanceLookAndFeel;
import org.jvnet.substance.skin.SubstanceModerateLookAndFeel;
import org.jvnet.substance.utils.SubstanceConstants;
import org.jvnet.substance.watermark.SubstanceImageWatermark;

import javax.swing.*;
import java.awt.*;

public class App {

    public static void main(String[] args) {
        JFrame.setDefaultLookAndFeelDecorated(true);
        JDialog.setDefaultLookAndFeelDecorated(true);
        try {
            //UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            UIManager.setLookAndFeel(new SubstanceModerateLookAndFeel());
            //SubstanceLookAndFeel.setCurrentTheme(new SubstanceTerracottaTheme());
            //SubstanceLookAndFeel.setCurrentButtonShaper(new StandardButtonShaper());
            SubstanceImageWatermark watermark = new SubstanceImageWatermark("images/1.jpg");
            SubstanceImageWatermark.setKind(SubstanceConstants.ImageWatermarkKind.SCREEN_TILE);
            SubstanceLookAndFeel.setCurrentWatermark(watermark);
        } catch (Exception e) {
            LogUtils.warn(e);
        }
        UIUtils.initGlobalFont(new Font("华文中宋", Font.PLAIN, 14));
        LogUtils.info("程序启动");
        MainFrame.init();
    }
}
