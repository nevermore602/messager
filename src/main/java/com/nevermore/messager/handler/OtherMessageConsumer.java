package com.nevermore.messager.handler;

import com.nevermore.messager.bean.MessageInfo;
import com.nevermore.messager.util.Config;
import com.nevermore.messager.util.LogUtils;
import com.nevermore.messager.util.MessageUtils;
import com.nevermore.messager.util.TaskUtils;

public class OtherMessageConsumer implements Runnable {
    @Override
    public void run() {
        LogUtils.debug("开启其他消息消费线程");
        while (Config.STATUS.equals(Config.STATUS_RUNNING)) {
            try {
                MessageInfo otherMessageInfo = TaskUtils.takeOtherMessage();
                MessageUtils.handleOtherMessage(otherMessageInfo);
            } catch (InterruptedException e) {
                LogUtils.warn("消费其他消息时出现异常！", e);
            }
        }
    }
}
