package com.nevermore.messager.handler;

import com.nevermore.messager.util.Config;
import com.nevermore.messager.util.IOUtils;
import com.nevermore.messager.util.LogUtils;
import com.nevermore.messager.util.UIUtils;

import javax.sound.sampled.*;
import java.io.IOException;
import java.net.BindException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;


/**
 * 接收语音数据的线程，输送到耳机等接收设备
 *
 * @author zengjian
 */
public class VoiceReceiver implements Runnable {
    private static VoiceReceiver instance = null;
    private boolean flag = false;

    private VoiceReceiver() {
    }

    public static VoiceReceiver getInstance() {
        if (instance == null)
            instance = new VoiceReceiver();
        return instance;
    }

    public void run() {
        flag = true;
        DatagramSocket socket;
        try {
            socket = new DatagramSocket(Config.VOICE_UDP_PORT);
        } catch (BindException e) {
            LogUtils.warn("语音接收线程socket绑定失败！", e);
            flag = true;
            LogUtils.info("socket被占用，说明语音接收线程正阻塞在最后一次循环正准备退出，\n"
                    + "此时，本线程只需要设置flag的值为true，让该线程继续运行，而本线程直接退出即可");
            return;
        } catch (SocketException e) {
            LogUtils.warn("语音接收线程socket创建失败！", e);
            UIUtils.showErroMessage(null, "语音接收线程启动失败！");
            flag = false;
            return;
        }
        byte data[] = new byte[512];
        DatagramPacket packet = new DatagramPacket(data, data.length);

        AudioFormat format = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, 44100.0f, 16, 1, 2, 44100.0f, false);
        DataLine.Info info = new DataLine.Info(SourceDataLine.class, format);
        SourceDataLine sourceDataLine;
        try {
            sourceDataLine = (SourceDataLine) AudioSystem.getLine(info);
            int bufSize = 16384;
            sourceDataLine.open(format, bufSize);
        } catch (LineUnavailableException ex) {
            LogUtils.warn(ex);
            flag = false;
            return;
        }
        sourceDataLine.start();
        while (flag) {
            try {
                socket.receive(packet);
                sourceDataLine.write(packet.getData(), 0, packet.getLength());
            } catch (IOException e) {
                LogUtils.warn("语音接收异常", e);
                break;
            }
        }
        if (flag) {
            sourceDataLine.drain();
        }
        sourceDataLine.stop();
        IOUtils.closeQuietly(sourceDataLine);
        IOUtils.closeQuietly(socket);
        flag = false;
        LogUtils.info("退出receiver run");
    }

    public boolean isFlag() {
        return flag;
    }

    public void shutDown() {
        this.flag = false;
    }

}