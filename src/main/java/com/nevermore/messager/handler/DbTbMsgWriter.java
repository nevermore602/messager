package com.nevermore.messager.handler;

import com.nevermore.messager.bean.TbMsg;
import com.nevermore.messager.util.Config;
import com.nevermore.messager.util.DBUtils;
import com.nevermore.messager.util.LogUtils;
import com.nevermore.messager.util.TaskUtils;

import java.sql.SQLException;
import java.util.Collection;

public class DbTbMsgWriter implements Runnable {
    @Override
    public void run() {
        LogUtils.debug("开启消息入库线程");
        while (Config.STATUS.equals(Config.STATUS_RUNNING)) {
            try {
                Collection<TbMsg> tbMsgs = TaskUtils.takeMultiDbWriteTbMsgs(100);
                LogUtils.debug("本次获取到[%s]条入库数据", tbMsgs.size());
                DBUtils.batchInsertMsg(tbMsgs);
            } catch (InterruptedException | SQLException | ClassNotFoundException e) {
                LogUtils.warn("消息入库失败！！", e);
            }
        }
    }
}
