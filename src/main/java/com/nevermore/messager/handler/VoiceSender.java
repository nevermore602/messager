package com.nevermore.messager.handler;

import com.nevermore.messager.exception.MessageOverflowException;
import com.nevermore.messager.util.Config;
import com.nevermore.messager.util.IOUtils;
import com.nevermore.messager.util.LogUtils;

import javax.sound.sampled.*;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;


/**
 * 发送语音的类，话筒端
 *
 * @author zengjian
 */
public class VoiceSender implements Runnable {
    private static VoiceSender instance = null;
    private DatagramSocket socket;
    private boolean flag = false;  //语音是否进行的标记，false表示断开
    private String hostAddress;

    private VoiceSender() {
    }

    public static synchronized VoiceSender getInstance() {
        if (instance == null)
            instance = new VoiceSender();
        return instance;
    }

    @Override
    public void run() {
        flag = true;
        try {
            socket = new DatagramSocket();
        } catch (SocketException e) {
            LogUtils.error("语音发送socket创建失败！", e);
            return;
        }
        AudioFormat format = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, 44100.0f, 16, 1, 2, 44100.0f, false);
        DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
        TargetDataLine targetDataLine;
        try {
            targetDataLine = (TargetDataLine) AudioSystem.getLine(info);
            if (!targetDataLine.isOpen())
                targetDataLine.open(format, targetDataLine.getBufferSize());
        } catch (LineUnavailableException e) {
            LogUtils.warn(e);
            flag = true;
            LogUtils.info("说明可能是已经在运行中而被占用，故尝试设置flag为true然后退出");
            return;
        }

        int length = 512;
        LogUtils.info(String.valueOf(length));
        byte[] data = new byte[length];
        int readLen;
        targetDataLine.start();
        while (flag) {
            readLen = targetDataLine.read(data, 0, data.length);
            try {
                send(data, readLen);//发送出去
            } catch (Exception ex) {
                LogUtils.warn("语音发送异常", ex);
                break;
            }
        }
        targetDataLine.stop();
        IOUtils.closeQuietly(targetDataLine);
        IOUtils.closeQuietly(socket);
        socket = null;
        flag = false;
        LogUtils.info("退出sender run");
    }

    private void send(byte[] data, int length) throws MessageOverflowException, IOException {
        if (length > 512) {  //数据长度限制在512字节内
            throw new MessageOverflowException();
        }
        DatagramPacket packet = new DatagramPacket(data, length, InetAddress.getByName(this.hostAddress), Config.VOICE_UDP_PORT);
        socket.send(packet);
    }

    public boolean isFlag() {
        return flag;
    }

    public void setHostAddress(String hostAddr) {
        this.hostAddress = hostAddr;
    }

    public void shutDown() {
        this.flag = false;
    }

}