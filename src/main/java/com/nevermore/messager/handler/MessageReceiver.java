package com.nevermore.messager.handler;

import com.nevermore.messager.bean.Message;
import com.nevermore.messager.bean.MessageInfo;
import com.nevermore.messager.util.Config;
import com.nevermore.messager.util.LogUtils;
import com.nevermore.messager.util.MessageUtils;
import com.nevermore.messager.util.TaskUtils;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;

/**
 * 接收聊天信息的线程，当接收到信息就显示在主窗口的聊天信息面板上
 */
public class MessageReceiver implements Runnable {
    private static MessageReceiver instance = null;
    private DatagramSocket socket;
    private DatagramPacket packet;

    private MessageReceiver() throws SocketException {
        byte data[] = new byte[512];
        packet = new DatagramPacket(data, data.length);
        socket = new DatagramSocket(new InetSocketAddress(Config.LOCAL_HOST_ADDR, Config.MSG_UDP_PORT));

        for (int i = 0; i < Config.TALK_MESSAGE_CONSUMER_NUM; i++) {
            TaskUtils.execute(new TalkMessageConsumer());
        }

        for (int i = 0; i < Config.OTHER_MESSAGE_CONSUMER_NUM; i++) {
            TaskUtils.execute(new OtherMessageConsumer());
        }

        for (int i = 0; i < Config.TB_MSG_CONSUMER_NUM; i++) {
            TaskUtils.execute(new DbTbMsgWriter());
        }
    }

    public static synchronized MessageReceiver getInstance() throws SocketException {
        if (instance == null) {
            instance = new MessageReceiver();
        }
        return instance;
    }


    @Override
    public void run() {
        while (Config.STATUS.equals(Config.STATUS_RUNNING)) {
            try {
                socket.receive(packet);
                String hostAddress = packet.getAddress().getHostAddress();
                Message msg = MessageUtils.parseMessage(packet.getData(), packet.getLength());
                LogUtils.receiveLog("收到消息：%s, %s", hostAddress, msg.toString());
                MessageInfo messageInfo = MessageInfo.builder().hostAddr(hostAddress).message(msg).build();
                try {
                    if (msg.getCmd() == Config.CMD_TALK_MSG) {
                        TaskUtils.putTalkMessage(messageInfo);
                    } else {
                        TaskUtils.putOtherMessage(messageInfo);
                    }
                } catch (InterruptedException e) {
                    LogUtils.warn(e);
                }
            } catch (IOException e) {
                LogUtils.warn("接收消息出错！", e);
            }
        }
    }
}