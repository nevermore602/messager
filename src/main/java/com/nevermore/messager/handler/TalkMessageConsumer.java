package com.nevermore.messager.handler;

import com.nevermore.messager.bean.MessageInfo;
import com.nevermore.messager.util.Config;
import com.nevermore.messager.util.LogUtils;
import com.nevermore.messager.util.MessageUtils;
import com.nevermore.messager.util.TaskUtils;

public class TalkMessageConsumer implements Runnable {

    @Override
    public void run() {
        LogUtils.debug("开启聊天消息消费线程");
        while (Config.STATUS.equals(Config.STATUS_RUNNING)) {
            try {
                MessageInfo talkMessageInfo = TaskUtils.takeTalkMessage();
                MessageUtils.handleTalkMessage(talkMessageInfo);
            } catch (InterruptedException e) {
                LogUtils.warn("消费聊天消息时出现异常！", e);
            }
        }
    }
}
