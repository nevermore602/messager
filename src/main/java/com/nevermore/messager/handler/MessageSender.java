package com.nevermore.messager.handler;

import com.nevermore.messager.bean.Message;
import com.nevermore.messager.exception.MessageOverflowException;
import com.nevermore.messager.util.Config;
import com.nevermore.messager.util.LogUtils;
import com.nevermore.messager.util.SysUtils;
import com.nevermore.messager.util.UIUtils;

import java.io.IOException;
import java.net.*;


/**
 * 发送消息的类
 *
 * @author zengjian
 */
public class MessageSender {
    private static DatagramSocket socket;

    public static void init() throws SocketException {
        socket = new DatagramSocket(new InetSocketAddress(Config.LOCAL_HOST_ADDR, 0));
    }

    public static void send(Message msg, String host) throws IOException {
        byte data[] = msg.toBytes();
        // 如果数据太大，要拆包发送
        if (msg.getAllByteLen() > 512) {
            int maxDataLen = 512 - msg.getHeaderByteLen() - msg.getOtherByteLen();
            byte subNum = (byte) ((msg.getDataByteLen() / maxDataLen) + (512 % maxDataLen == 0 ? 0 : 1));
            LogUtils.sendLog("需拆分的消息：%s", msg.toString());
            LogUtils.sendLog("最大数据长度：%s，分%s次发送", maxDataLen, subNum);
            for (byte i = 0; i < subNum; i++) {
                Message subMsg = msg.buildSubMessage(subNum, i, maxDataLen);
                send(subMsg, host);
            }
        } else {
            LogUtils.sendLog("发送消息：%s, %s", host, msg.toString());
            DatagramPacket packet = new DatagramPacket(data, data.length, InetAddress.getByName(host), Config.MSG_UDP_PORT);
            socket.send(packet);
        }
    }

    public static void sendLoginBroadcast() {
        String userName = SysUtils.getLoginUserName(); //此处暂时取主机登录用户名
        Message msg = new Message(Config.CMD_LOGIN_MSG, userName, Config.LOCAL_HOST_NAME, "LoginBroadcast");
        try {
            MessageSender.send(msg, Config.BROADCAST_ADDR);
        } catch (Exception e) {
            LogUtils.warn("登陆广播发送失败！", e);
            UIUtils.showErroMessage(null, "登陆广播发送失败");
        }
    }


    public static void sendLogoutBroadcast() {
        String userName = SysUtils.getLoginUserName(); //此处暂时取主机登录用户名
        Message msg = new Message(Config.CMD_LOGOUT_MSG, userName, Config.LOCAL_HOST_NAME, "LogoutBroadcast");
        try {
            MessageSender.send(msg, Config.BROADCAST_ADDR);
        } catch (Exception e) {
            LogUtils.warn("退出广播发送失败！", e);
        }
    }

    public static void sendLoginReplyMsg(String hostAddress) {
        String userName = SysUtils.getLoginUserName(); //此处暂时取主机登录用户名
        Message replyMsg = new Message(Config.CMD_LOGIN_REPLY_MSG, userName, Config.LOCAL_HOST_NAME, "LoginBroadcastReply");
        try {
            MessageSender.send(replyMsg, hostAddress);
        } catch (Exception e) {
            LogUtils.warn("登陆回复消息发送失败！", e);
        }
    }

    public static void sendShakeMsg(String hostAddress) {
        String userName = SysUtils.getLoginUserName(); //此处暂时取主机登录用户名
        Message shakeMsg = new Message(Config.CMD_SHAKE, userName, Config.LOCAL_HOST_NAME, "Shake");
        try {
            MessageSender.send(shakeMsg, hostAddress);
        } catch (Exception e) {
            LogUtils.warn("抖动消息发送失败！", e);
        }
    }

    public static void sendRequestVoiceMsg(String hostAddress) {
        String userName = SysUtils.getLoginUserName(); //此处暂时取主机登录用户名
        Message reqVoiceMsg = new Message(Config.CMD_REQUEST_VOICE, userName, Config.LOCAL_HOST_NAME, "Request_Voice");
        try {
            MessageSender.send(reqVoiceMsg, hostAddress);
        } catch (Exception e) {
            LogUtils.warn("请求语音消息发送失败！", e);
        }
    }

    public static void sendStopVoiceMsg(String hostAddress) {
        String userName = SysUtils.getLoginUserName(); //此处暂时取主机登录用户名
        Message stopVoiceMsg = new Message(Config.CMD_STOP_VOICE, userName, Config.LOCAL_HOST_NAME, "StopVoice");
        try {
            MessageSender.send(stopVoiceMsg, hostAddress);
        } catch (Exception e) {
            LogUtils.warn("中断语音消息发送失败！", e);
        }
    }

    /**
     * @param port          文件传送的端口
     * @param hostAddress   文件名字符串和长度字符串组合时以“*”连接
     * @param fileNameStr   封装多个文件名的组合字符串，以符合“|”分隔
     * @param fileLengthStr 封装多个文件名的长度字符串，以符合“|”分隔
     */
    public static void sendRequestFileSendMsg(String hostAddress, int port, String fileNameStr, String fileLengthStr) throws MessageOverflowException, IOException {
        String userName = SysUtils.getLoginUserName();
        Message msg = new Message(Config.CMD_REQUEST_SEND_FILE, userName, Config.LOCAL_HOST_NAME, port + "*" + fileNameStr + "*" + fileLengthStr);
        MessageSender.send(msg, hostAddress);
    }

    public static void sendRefuseFileMsg(String hostAddress) {
        String userName = SysUtils.getLoginUserName();
        Message refuseFileMsg = new Message(Config.CMD_REFUSE_FILE, userName, Config.LOCAL_HOST_NAME, "RefuseFile");
        try {
            MessageSender.send(refuseFileMsg, hostAddress);
        } catch (Exception e) {
            LogUtils.warn("拒绝接收文件消息发送失败！", e);
        }
    }

    public static void sendCancelReciveFileMsg(String hostAddress) {
        String userName = SysUtils.getLoginUserName();
        Message cancelReceiveFileMsg = new Message(Config.CMD_CANCEL_RECEIVE_FILE, userName, Config.LOCAL_HOST_NAME, "CancelReceiveFile");
        try {
            MessageSender.send(cancelReceiveFileMsg, hostAddress);
        } catch (Exception e) {
            LogUtils.warn("取消接收文件消息发送失败！", e);
        }
    }

    public static void sendCancelSendFileMsg(String hostAddress) {
        String userName = SysUtils.getLoginUserName();
        Message cancelSendFileMsg = new Message(Config.CMD_CANCEL_SEND_FILE, userName, Config.LOCAL_HOST_NAME, "CancelSendFile");
        try {
            MessageSender.send(cancelSendFileMsg, hostAddress);
        } catch (Exception e) {
            LogUtils.warn("取消发送文件消息发送失败！", e);
        }
    }

    public static void sendRefuseVoiceMsg(String hostAddress) {
        String userName = SysUtils.getLoginUserName();
        Message refuseVoiceMsg = new Message(Config.CMD_REFUSE_VOICE, userName, Config.LOCAL_HOST_NAME, "RefuseVoice");
        try {
            MessageSender.send(refuseVoiceMsg, hostAddress);
        } catch (Exception e) {
            LogUtils.warn("拒绝语音消息发送失败！", e);
        }
    }

    public static void sendAcceptVoiceMsg(String hostAddress) {
        String userName = SysUtils.getLoginUserName();
        Message acceptVoiceMsg = new Message(Config.CMD_ACCEPT_VOICE, userName, Config.LOCAL_HOST_NAME, "AcceptVoice");
        try {
            MessageSender.send(acceptVoiceMsg, hostAddress);
        } catch (Exception e) {
            LogUtils.warn("接收语音消息发送失败！", e);
        }
    }

    //Test
    public static void main(String[] args) throws Exception {
        String targetHost = "192.168.0.118";
        Config.LOCAL_HOST_ADDR = "192.168.0.110";
        MessageSender.init();
        String userName = "张三";
        for (int i = 0; i < 100; i++) {
            send(new Message(Config.CMD_TALK_MSG, userName, Config.LOCAL_HOST_ADDR, "" + i), targetHost);
            Thread.sleep(10);
        }
    }

}